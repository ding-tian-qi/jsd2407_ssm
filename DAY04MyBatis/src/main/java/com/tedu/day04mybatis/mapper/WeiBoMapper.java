package com.tedu.day04mybatis.mapper;

import com.tedu.day04mybatis.jopo.WeiBo;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface WeiBoMapper {
    @Insert("insert into weibo(content,created,user_id)values(#{content},#{created},#{userId})")
    int insert(WeiBo weiBo);
    @Delete("delete from weibo where id=#{id}")
    int deleteById(Integer id);
    @Update("update weibo set content=#{content},user_id=#{userId} where id=#{id}")
    int updateById();
    @Select("select id,content,created,user_id userId from weibo where id=#{id}")
    WeiBo findById(Integer id);
    @Select("select id,content,created,user_id userId from weibo")
    List<WeiBo> findAll();
}
