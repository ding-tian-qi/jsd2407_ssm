package com.tedu.day04mybatis.mapper;

import com.tedu.day04mybatis.jopo.Student;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StudentMapper {
    @Insert("insert into student(name,gender,age)values(#{name},#{gender},#{age})")//values后面的名字要和Student类的属性名字相同
    int insert(Student student);
}
