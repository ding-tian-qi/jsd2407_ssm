package com.tedu.day04mybatis.mapper;

import com.tedu.day04mybatis.jopo.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper//有这个标志就说明MyBatis中的动态代理类可以作为这个类的实现类，动态代理类是运行起来才确定他的实现类
public interface UserMapper {
    @Insert("INSERT INTO user(username,password,nickname,created,age)values(#{username},#{password},#{nickname},#{created},#{age})")
    int insert(User user);
    @Delete("delete from user where username=#{username}")
    int deleteByName(String name);
}
/**
 * 使用MyBatis步骤
 *  1.定义pojo类
 *          ORM映射类，定一个类，对应数据库中某张表
 *          类名尽量与表名一直
 *          属性名与字段名一致
 *          属性提供getset方法
 *  2.定义一个接口Mapper接口
 *          该接口需要添加注解，@Mapper
 *          定义对应操作的方法，比如insert方法，方法参数需要传入像表中插入数据所对应的对象pojo
 *          方法上添加对应操作的注解，例如insert操作应当添加@Insert注解，注解参数是对应的insert语句
 *          对应的值的站位 使用#{},其中指定属性的名字
 *  3.在测试类中定义Mapper属性，添加自动装配注解，用于让Spring注入Mapper实现，（通过动态代理实现的）
 *
 *  4.在测试类中添加一个单元测试方法，并添加一个@Test注解，
 *          实例化一个pojo对象，并为属性设置好对应的值，
 *          调用Mapper对应方法将pojo对象传入，进行操作
 *
 */
