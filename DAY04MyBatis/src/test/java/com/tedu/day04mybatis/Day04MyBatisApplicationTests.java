package com.tedu.day04mybatis;

import com.tedu.day04mybatis.jopo.Student;
import com.tedu.day04mybatis.jopo.User;
import com.tedu.day04mybatis.jopo.WeiBo;
import com.tedu.day04mybatis.mapper.MyMapper;
import com.tedu.day04mybatis.mapper.StudentMapper;
import com.tedu.day04mybatis.mapper.UserMapper;
import com.tedu.day04mybatis.mapper.WeiBoMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@SpringBootTest
class Day04MyBatisApplicationTests {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private StudentMapper studentMapper;
    @Autowired
    private WeiBoMapper weiBoMapper;
    @Test
    void testInsert(){
        User user=new User();
        user.setUsername("王伟超");
        user.setPassword("123456");
        user.setNickname("伟超");
        user.setCreated(new Date());
        user.setAge(22);

        //MyMapper mapper=new MyMapper();
        //int num = mapper.insert(user);
        int num=userMapper.insert(user);
        System.out.println(num>0?"插入成功":"插入失败");
    }
    @Test
    void insert() {
        Student student = new Student();
        student.setName("丁天奇");
        student.setAge(22);
        student.setGender("男");
        int num = studentMapper.insert(student);
        System.out.println(num > 0 ? "插入成功" : "插入失败");
    }
    @Test
    void insertWeiBo(){
        WeiBo weiBo=new WeiBo();
        weiBo.setContent("今天周五");
        weiBo.setCreated(new Date());
        weiBo.setUserId(3);
        int num=weiBoMapper.insert(weiBo);
        System.out.println(num > 0 ? "插入成功" : "插入失败");
    }
    @Test
    void deleteweibo(){
        int num=weiBoMapper.deleteById(2);
        System.out.println(num>0?"删除成功":"删除失败");
    }
    @Test
    void deleteByname(){
        int num=userMapper.deleteByName("王伟超");
        System.out.println(num>0?"删除成功":"删除失败");
    }
    @Test
    void testUpdateWeibo(){
        WeiBo weiBo=new WeiBo();
        weiBo.setId(3);
        weiBo.setContent("我不是丁老湿，我是大聪明");
        weiBo.setUserId(7);
        int num = weiBoMapper.updateById();
        System.out.println(num>0?"更新成功":"更新失败");

    }
    /*
        根据id查找信息
     */
    @Test
    void test(){
        WeiBo weiBo=new WeiBo();
       WeiBo wb= weiBoMapper.findById(3);
        System.out.println(wb);

    }
    @Test
    void findAll(){
        List<WeiBo> list = weiBoMapper.findAll();
        for(WeiBo w:list){
            System.out.println(w);
        }
    }

}
