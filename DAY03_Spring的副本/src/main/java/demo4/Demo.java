package demo4;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * spring自动装配，面向接口编程
 * 看看食堂怎么给我么做饭的
 */
public class Demo {
    public static void main(String[] args) {
        //初始化Spring容器
        AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext("demo4");
        Person person = context.getBean(Person.class);
        System.out.println(person);
    }
}
