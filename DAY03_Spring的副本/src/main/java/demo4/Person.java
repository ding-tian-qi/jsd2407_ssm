package demo4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * 依赖关系
 *
 * 当一个类实现了某种功能时，使用了另一个类，那么此时可以说当前类与另一个类有依赖关系；
 * 通过属性设置类与类之间的依赖关系叫做依赖注入；----在一个类中用到哪个类，就把哪个类定义成这个类的属性
 *
 *
 * 依赖注入----面向接口编程
 *
 * 依赖的是一类事物，而不是某个固定的一个事物！！！！
 */
@Component
public class Person {
    /**
     * 该注解用于定义在属性上，告知spring该属性需要自动装配
     * 此时Spring会根据属性的类型扫描包中的类，找到合适的类并初始化他
     * 然后通过该属性为其注入 对他的依赖
     */
    @Autowired//因为Shirt要的不是一个具体的值，因此不能用@Value()，所以要用@Autowired自动装配注解
    @Qualifier("winter")//如果Spring在自动装配时，发现多个可以装配 的类时
                        //例如clothes接口中有两个实现类：shirt和sweater  那么如果没有特别说明，Spring会在装配时抛出异常
                        //此时我们的做法有两种
                        //1.为依赖这里添加@Qualifier限定依赖的组件
                            //此时
                        //2.避免在Spring中出现可以匹配多个实现类的情况
    //private Shirt shirt;
    //private Sweater sweater;
    private Clothes clothes;

    @Override
    public String toString() {
        return "Person{" +
                "clothes=" + clothes +
                '}';
    }
}
