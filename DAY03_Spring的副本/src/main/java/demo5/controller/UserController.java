package demo5.controller;

import demo5.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Controller将来用户和用户打交道（与浏览器交互部分）类似于前台；
 */
@Component
public class UserController {
    @Autowired
    private IUserService userService;
    public void doLogin(){
        System.out.println("UserController:收到用户请求，要求登陆");
        boolean success = userService.login("张三","123456");
        if(success){
            System.out.println("UserController:回馈用户登陆成功");
        }else{
            System.out.println("UserController:回馈用户登陆失败提示");
        }
    }
}
