package demo5.dao;

import org.springframework.stereotype.Repository;

public interface IUserDAO{
    public boolean findUser();
}
