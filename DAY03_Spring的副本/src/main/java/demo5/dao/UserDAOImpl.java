package demo5.dao;

import org.springframework.stereotype.Repository;

@Repository
public class UserDAOImpl implements IUserDAO{
    @Override
    public boolean findUser() {
        System.out.println("UserDAO:根据用户名和密码查找用户信息");
        double r=Math.random();
        System.out.println("用户信息："+r);
        if(r>0.5){
            System.out.println("UserDAO:找到了该用户将其返回");
            return true;
        }else{
            System.out.println("UserDAO:未找到该用户！");
            return false;
        }
    }
}
