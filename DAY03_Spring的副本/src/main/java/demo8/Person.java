package demo8;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class Person {
    private String name;

    public Person() {
        System.out.println("Person的无参构造器执行了");
    }

    public String getName() {
        return name;
    }
    @Value("丁老湿")
    public void setName(String name) {
        System.out.println("Person的setName()方法初始化name属性执行了....");
        this.name = name;
    }
    @PostConstruct
    //一旦一个方法被注解@PostConstruct标注，那么Spring在实例化对象后会调用这个方法一次
    public void init(){
        System.out.println("Person的init方法被调用了，对象创建了");
    }
    @PreDestroy
    //Spring在销毁一个对象前会调用该方法（理解为该对象生命周期最后一个被调用的方法，调用后对象就会被销毁）
    public void destroy(){
        System.out.println("Person的destroy方法被调用了，对象销毁了");
    }

    /**
     *  Person.finalize();
     *  finalize() 是Object中定义的一个方法，当GC法案一个对象没有引用时，会释放该对象
     *  那么释放前会调用一次该方法，这个方法调用后对象被释放，这意味着该方法是一个对象声明周期真正的最后一个方法
     */

}
