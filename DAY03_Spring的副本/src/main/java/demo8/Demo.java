package demo8;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Demo {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext("demo8");
        Person person = context.getBean(Person.class);
        System.out.println("Spring返回了一个Person对象，可以开始使用该对象了");
        context.close();//关闭Spring容器，Spring会销毁所有管理的对象
    }
}
