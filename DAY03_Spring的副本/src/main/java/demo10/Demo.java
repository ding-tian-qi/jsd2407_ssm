package demo10;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Configuration
 * 将Demo类定义成一个配置类，
 *      传给Spring后，Spring就会从这个配置类中获取一些配置信息，用来加载容器初始化容器属性
 *
 * @ComponentScan
 * 该注解用于告知Spring容器扫喵哪个包中的类进行管理
 *      Spring容器在创建时会将该包以及子包中所有被@Component等注解标注的类注册为Spring Bean进行管理
 */
@Configuration
@ComponentScan("demo10")
public class Demo {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext(Demo.class);//直接将配置类的类对像传进来就可以了，就不用写包名了
        Person person=context.getBean(Person.class);
        System.out.println(person);
    }
}
