package demo6;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 测试@Resource注解----和@Autowired注解一样----都是自动装配
 * @Resource(name="类名") 可以有参数  注意：类名的首字母必须要求“小写！！！！”
 */
public class Demo {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext("demo6");
        Person person = context.getBean(Person.class);
        System.out.println(person);
    }
}
