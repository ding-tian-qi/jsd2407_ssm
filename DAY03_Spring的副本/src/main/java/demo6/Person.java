package demo6;

import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Resource 不是Spring框架的注释，是java自己的注释
 */
@Component
public class Person {
    //@Resource//java自己的注释，不是Spring里面的注释；
    //private Clothes clothes;
    //@Resource(name="shirt")
    //private Clothes clothes;
    @Resource
    private Clothes shirt;//weather//可以直接修改属性的名

    @Override
    public String toString() {
        return "Person{" +
                "clothes=" + shirt +//weather
                '}';
    }
}
