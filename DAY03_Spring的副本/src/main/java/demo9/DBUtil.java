package demo9;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * resource 和 java 是同一个目录，加载完后 会放在classes中
 *
 * Bean容器就是类容器  Bean就是类
 */
@Component
//用来告诉spring容器对应的properties文件在哪里，让其去读取
@PropertySource("classpath:config.properties")//classpath：---表示从target中的classes开始
public class DBUtil {
    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("{spring.datasource.password}")
    private String password;

    @Override
    public String toString() {
        return "DBUtil{" +
                "url='" + url + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
