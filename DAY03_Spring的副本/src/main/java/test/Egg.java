package test;

import org.springframework.stereotype.Component;

@Component("egg")
public class Egg implements Food{
    @Override
    public String toString() {
        return "鸡蛋";
    }
}
