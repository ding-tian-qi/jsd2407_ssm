package test;

import org.springframework.stereotype.Component;
@Component("hamburger")
public class Hamburger implements Food{
    @Override
    public String toString() {
        return "汉堡包";
    }
}
