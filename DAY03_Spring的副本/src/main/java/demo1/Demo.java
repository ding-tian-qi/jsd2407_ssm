package demo1;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 使用spring容器来管理项目中要使用的所有类
 * 我们将创建，初始化类等操作的控制权转交给spring容器来管理，被成为控制反转 IOC（Inverse of Control）
 */
public class Demo {
    public static void main(String[] args) {
        //Spring创建

        //想要spring管理这个类，必须使用@component进行注解
        AnnotationConfigApplicationContext context
                =new AnnotationConfigApplicationContext("demo1");//认识一下----要先让他扫描我们的包，让他认识一下包中的类
        User user2=context.getBean(User.class);//NoSuchBeanDefinitionException
        user2.sayHello();
    }
}
