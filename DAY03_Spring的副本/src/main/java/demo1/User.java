package demo1;

import org.springframework.stereotype.Component;

/*
    组件，有了该注解的类就是被Spring框架管理的类 @Component
*/
@Component//组件，组成配件，零件
public class User {
    public void sayHello(){
        System.out.println("hello!");
    }
}
