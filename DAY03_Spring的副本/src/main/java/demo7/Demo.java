package demo7;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Spring管理对象时：单例或多例
 *
 * 对类的默认维护是 单例模式，使用该模式设计的类全局只有一个实例对象
 *
 *
 *
 *
 */
public class Demo {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext("demo7");
        DBConnect db1=context.getBean(DBConnect.class);
        System.out.println(db1);
        DBConnect db2=context.getBean(DBConnect.class);
        System.out.println(db2);
    }
}
