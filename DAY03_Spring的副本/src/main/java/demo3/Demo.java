package demo3;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *  使用spring的时候没有加注解，出现的异常：
 *      NoSuchBeanDefinitionException
 */
public class Demo {
    public static void main(String[] args) {
        UserDAO userdao=new UserDAO();
        System.out.println(userdao);

        //利用Spring的方式----在属性上直接加注解
        AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext("demo3");
        UserDAO userDAO=context.getBean(UserDAO.class);
        System.out.println(userDAO);
        //利用Spring的方式----在属性的set方法上加注解
        Person p2=context.getBean(Person.class);
        System.out.println(p2);
    }
}
