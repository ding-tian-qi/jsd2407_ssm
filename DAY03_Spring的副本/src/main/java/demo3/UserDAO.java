package demo3;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

/**
 * DAO   DATA Access Object
 *  DAO是项目分层中的一个分层名，是一类专门与数据库打交道的类的总称
 *  这些类在与数据库打交道时，希望用面向对象的思想，将数据库中的数据进行表达
 *  例如：
 *      类：表
 *      对象：表中的一条记录
 *      类属性：表中的字段
 *  希望程序用对象的形式表示表中的数据来对数据库操作，以上思想目前知道即可，后面学习Mybatis时理解，orm映射
 */
@Repository
public class UserDAO {
    //连接数据库时的地址
    /**
     * @Value 注解可以被标注在属性上，Spring在创建对象时会同时根据该注解的参数来初始化该属性的值
     */
    @Value("jdbc:mysql://localhost:3306/tedu")//url属性会被赋值为@Value()注解参数的值
    private String url;
    @Value("root")
    //数据库用户名
    private String username;
    //数据库密码
    @Value("123456abc")
    private String password;

    @Override
    public String toString() {
        return "UserDAO{" +
                "url='" + url + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
