package demo2;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * 四个注解都可以使用，如果被注解的类没有分层，哪一个注解都可以
 */
//@Repository
//@Service
//@Controller
@Component
public class User {
    public void sayHello(){
        System.out.println("Hello!!!");
    }
}
