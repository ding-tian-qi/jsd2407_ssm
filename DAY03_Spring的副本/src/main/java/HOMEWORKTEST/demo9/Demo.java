package HOMEWORKTEST.demo9;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Demo {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext("HOMEWORKTEST.demo9");
        MyClazz c=context.getBean(MyClazz.class);
        System.out.println(c);
    }
}
