package HOMEWORKTEST.demo9;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:my.properties")
public class MyClazz {
    @Value("${app.user.name}")
    private String name;
    @Value("${spring.datasource.age}")
    private int age;
    @Value("${app.user.gender}")
    private String gender;

    @Override
    public String toString() {
        return "MyClazz{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                '}';
    }
}
