package HOMEWORKTEST.demo02;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 *    将java类中的@Repository注解，替换为 @Component注解，@Service注解，@Controller注解都可以
 *
 * @Component 该注解用于描述Spring中的Bean，他是一个泛化的概念，仅仅标示容器中的一个组件Bean，并且可以作用在任何层次
 *            使用时只需要将该注解标注在相应的类上即可
 * @Repository 该注解用于数据访问层（Dao层）的类标识为Spring中的Bean，功能与@Component相同
 *
 * @Service 该注解通常作用在业务层，将业务层的类标示为Spring中的Bean，功能与@Component相同
 *
 * @Controller 该注解作用在控制层 ，用于将控制层的类标示为Spring中的Bean，功能与@Component相同
 */
@Repository
public class User {
    public void sayHello(){
        System.out.println("hello!!");
    }
}
