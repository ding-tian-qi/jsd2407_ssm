package HOMEWORKTEST.demo02;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *  demo02做了什么？
 *  测试了其他三个Bean注解的功能和@Component注解的功能一样
 *  唯一的区别是，可以在不同的层面使用其他三个注解，@Component注解什么地方都可以用
 *
 */
public class Demo {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext("HOMEWORKTEST");
        User bean = context.getBean(User.class);
        bean.sayHello();
    }
}
