package HOMEWORKTEST.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Student {
    @Autowired
    @Qualifier("鸡蛋")
    private Food food;

    @Override
    public String toString() {
        return "Student{" +
                "food=" + food +
                '}';
    }
}
