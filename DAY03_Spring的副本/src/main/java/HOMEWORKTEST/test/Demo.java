package HOMEWORKTEST.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Demo {
    public static void main(String[] args) {
        ApplicationContext context=new AnnotationConfigApplicationContext("HOMEWORKTEST.test");
        Student s=context.getBean(Student.class);
        System.out.println(s);
    }
}
