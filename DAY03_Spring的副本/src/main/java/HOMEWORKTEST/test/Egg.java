package HOMEWORKTEST.test;

import org.springframework.stereotype.Component;

@Component("鸡蛋")
public class Egg implements Food{
    @Override
    public String toString() {
        return "鸡蛋";
    }
}
