package HOMEWORKTEST.test;

import org.springframework.stereotype.Component;

@Component("汉堡包")
public class Hamburger implements Food{
    @Override
    public String toString() {
        return "汉堡包";
    }
}
