package HOMEWORKTEST.demo04;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Person {
    /**
     * 该注解用于定义在属性上，告知Spring该属性需要自动装配
     * 此时Spring会根据属性的类型扫描包中的类，找到合适的类并初始化它
     * 然后通过该属性为其“注入”对他的依赖
     */
    @Autowired()
    @Qualifier("冬天")
    private Clothes clothes;

    @Override
    public String toString() {
        return "Person{" +
                "clothes=" + clothes +
                '}';
    }
}
