package HOMEWORKTEST.demo04;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.lang.annotation.Annotation;

/**
 * 演示自动装配
 *
 * 依赖关系
 *      当一个类在实现某种功能时，使用了另一个类，那么此时可以说当前类与另一个有依赖关系
 *      Spring建议我们将当前类依赖的类都可以定义为属性
 *
 * 依赖注入：
 *   那么在创建当前类时，Spring会将依赖的类自动装配到该属性上，
 *   而这种通过属性将依赖的对象设置到当前对象的过程称为”依赖注入“
 */

public class Demo {
    public static void main(String[] args) {
        ApplicationContext context=new AnnotationConfigApplicationContext("HOMEWORKTEST.demo04");
        Person p= context.getBean(Person.class);
        System.out.println(p);
    }
}
