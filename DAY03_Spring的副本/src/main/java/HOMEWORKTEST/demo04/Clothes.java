package HOMEWORKTEST.demo04;

import org.springframework.stereotype.Component;

/**
 * 面向接口，是一种编程上的设计思想
 * 当一个类依赖另一个类，但是依赖的这个类可能由多种不同的实现
 * 那么我们通常不会直接依赖某一个具体的实现类，而是依赖接口
 * 那样以来，只要被依赖的类也实现了该接口，我们就可以使用这个类
 *
 * 就好比人出门需要依赖衣服，而不是必须依赖衬衫，或者毛衣，任何中类的衣服都可以
 * 因此Person上应该定义一个属性时，Clothes这个接口，而不是具体的衬衫或者毛衣
 */
@Component
public interface Clothes {
}
