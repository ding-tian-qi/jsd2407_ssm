package HOMEWORKTEST.demo01;

import org.springframework.stereotype.Component;

/**
 * Spring框架提供的一个注释@Component
 * 被该注释标注的类才会被Spring框架管理，没有被该注解标注的类
 * 会被Spring框架自动忽略
 */
@Component
public class User {
    public void sayHello(){
        System.out.println("hello");
    }
}
