package HOMEWORKTEST.demo01;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *  使用Spring容器来管理项目中要使用的所有类
 *  我们将创建，初始化类等操作的控制权交给Spring容器来管理，称为控制反转 IOC
 */

/**
 * demo01做了什么？
 * 1.创建了一个User类，里面输出hello。使用Spring注解@Component标注这个类
 * 2.在demo里面使用Spring提供的接口
 * ApplicationContext context=new AnnotationConfigApplicationContext("指定要Spring扫描的包名")创建容器
 * 3.使用context容器对象的getBean()方法实例化类对象 context.getBean(类的.class)
 * 4.通过实例化的类对象调用类的方法  user.sayHello();
 */
public class Demo {
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        //曾经我们创建对象，调用方法的写法
        User user=new User();//缺点，写死了，换个别的类或实现就要改代码
                             //这里就只能new  User类 换别的类就要重写new
        user.sayHello();

        //前段时间学习了反射机制
        /*Class cls=Class.forName("demo01.User");//好处是：还类时程序不改了
        User boj=(User)cls.newInstance();//缺点，写一堆反射代码
        boj.sayHello();*/

        /*
            如今我们学习了Spring 框架
            Spring框架提供的核心类，用于管理项目中的类
            指定让Spring框架扫描的包，框架便会扫描该包下面的所有类以及这个包下面的所有子包中的类
         */
        ApplicationContext context=new AnnotationConfigApplicationContext("HOMEWORKTEST");
        User bean = context.getBean(User.class);
        bean.sayHello();
    }
}
