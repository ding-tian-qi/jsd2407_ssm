package HOMEWORKTEST.demo03;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * demo03主要演示了使用框架后的创建对象和赋值
 * 和以前创建对象赋值的一个区别
 */
public class Demo {
    public static void main(String[] args) {
        //没有Spring前，创建对象
        UserDAO userdao = new UserDAO();
        System.out.println(userdao);
        //使用框架创建对象，并且同时赋值
        AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext("HOMEWORKTEST.demo03");
        UserDAO bean = context.getBean(UserDAO.class);
        System.out.println(bean);

        //没有Spring框架前，需要手动创建对象赋值
        Person p=new Person();
        p.setName("王克晶");
        p.setAge(22);
        System.out.println(p);

        //有了Spring框架后，只需要使用容器来管理创建对象和赋值就可以了
        Person p2=context.getBean(Person.class);
        System.out.println(p2);

    }
}
