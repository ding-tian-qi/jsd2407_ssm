package HOMEWORKTEST.demo03;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Spring 可以通过属性的set()方法对属性进行赋值
 */
@Component
public class Person {
    private String name;
    private int age;

    public Person() {
    }

    public int getAge() {
        return age;
    }
@Value("22")
    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }
@Value("丁老湿")
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
