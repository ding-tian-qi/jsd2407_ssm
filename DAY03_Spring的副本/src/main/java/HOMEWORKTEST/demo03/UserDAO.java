package HOMEWORKTEST.demo03;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * DAO  data Access Object
 * 他是一个项目分封的一个分层名，是一类专门与数据库打交道的类的总称
 *      类------表
 *      对象-----表中的一条记录
 *      类属性----表中的字段
 *      希望程序用对象的形式标示表中的数据来对数据库操作
 *      以上思想目前知道即可
 *
 *
 *  数据库中
 *  Userinfo表
 *  ID     name     age    gender
 *  1       范传奇     22      男
 *  2       王克晶     33      女
 *  ..       ...        ..      ..
 *
 *  根据数据库中的表创建出一个类
 *   UserInfo类
 *      private int id
 *      private String name
 *      private int age
 *      private String gender
 *
 *    public void insert(UserInfo userinfo)
 *
 *    public UserInfo query(String name)
 *
 */
@Component
public class UserDAO {
    //连接数据库时的地址
    //@Value()注解可以被标注在属性上，Spring在创建对象时，会同时根据该注解的参数来初始化该属性的值
    @Value("www.baidu.com")
    private String url;
    @Value("root")
    private String name;
    @Value("123")
    private String password;

    @Override
    public String toString() {
        return "UserDAO{" +
                "url='" + url + '\'' +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
