package HOMEWORKTEST.dmeo7;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")//多利模式----单例模式：singleton
public class DBConnect {
}
