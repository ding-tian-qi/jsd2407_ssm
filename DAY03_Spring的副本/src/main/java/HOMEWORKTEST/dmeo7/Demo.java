package HOMEWORKTEST.dmeo7;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Demo {
    public static void main(String[] args) {
        //演示多例
        AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext("HOMEWORKTEST.dmeo7");
        DBConnect db=context.getBean(DBConnect.class);
        System.out.println(db);

        DBConnect db2=context.getBean(DBConnect.class);
        System.out.println(db2);
    }
}
