package HOMEWORKTEST.demo5.dao;

import org.springframework.stereotype.Component;

@Component
public class UserDAOImpl implements IUserDAO{
    public boolean findUser(){
        System.out.println("UserDAO:根据用户名，密码查找用户信息");
        double r=Math.random();
        if(r>0.5){
            System.out.println("UserDAO:找到该用户将其返回");
            return true;
        }else {
            System.out.println("UserDAO：找不到该用户");
            return false;
        }
    }
}
