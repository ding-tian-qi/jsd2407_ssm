package HOMEWORKTEST.demo5;

import HOMEWORKTEST.demo5.controller.UserController;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class demo5 {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext("HOMEWORKTEST.demo5");
        UserController controller=context.getBean(UserController.class);
        controller.doLogin();
    }
}
