package HOMEWORKTEST.demo5.service;

import HOMEWORKTEST.demo5.dao.IUserDAO;
import org.springframework.stereotype.Component;

@Component
public class UserServiceImpl implements IUserService{
    private IUserDAO userDAO;

    @Override
    public boolean login(String name, String password) {
        System.out.println("UserService:开始处理登陆操作");
        System.out.println("UserService:验证用户信息是否有误");
        System.out.println("UserService:获取用户信息...");
        boolean success=userDAO.findUser();
        if(success){
            System.out.println();
            System.out.println("UserService:用户登录成功");
        }else{
            System.out.println("UserService:用户登录失败");
        }
        return success;
    }
}
