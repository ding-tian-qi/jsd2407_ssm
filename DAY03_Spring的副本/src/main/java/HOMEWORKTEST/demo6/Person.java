package HOMEWORKTEST.demo6;

import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Resource 不是Spring框架提供的注解，是Java提供的注解
 *
 * Resource：依赖注入，注入对象或者接口类型的数据
 *
 *      装配机制：
 *          1.根据IOC容器中Spring Bean 对象的名称进行装配【name参数】
 *          2.未指定名称【name参数】，把属性名作为Bean对象名称进行装配
 *          3.属性名和Bean对象名称也不一致时，则根据类型进行装配
 *
 *      Autowired注解和Resource注解得区别：
 *          1.Autowired是Spring Framwork注解，Resource是JDK标准注解
 *          2.装配机制不同：
 *               2.1 Autowired注解根据类型进行装配
 *               2.2Resource注解
 *                      1）根据Bean对象名称进行装配
 *                      2）未指定名称，则使用属性名作为名称进行装配
 *                      3）属性名和Bean对象名也不一致时，根据类型进行装配
 *                      4）根据类型装配失败，抛出异常：NOUNiqueBeanDefinitionException
 *
 * @Resource
 * private Clothes clothes;
 *
 * @Resource(name="sweater")  类名注意，首字母必须小写，其余与类名完全一致即可
 * private Clothes clothes;
 */
@Component
public class Person {
    /**
     * 如果使用的是
     * @Resource
     * private Clothes shirt;
     * 没有指定任何名称，那么Spring就会尝试根据属性名也就是Clothes去IOC容器中
     * 寻找Clothes类型的Bean对象注入到这个属性中，如果Clothes中有多个实现类，那么就会报错，要么显示的指定name的值
     *
     *
     * 第三种情况：
     * @Resource
     * private Clothes clothes
     * 没有指定名称，Spring会尝试在IOC容器寻找Clothes类型的bean来注入到属性中
     */
    @Resource(name="shirt")
    //private Clothes shirt;
    private Clothes clothes;


    @Override
    public String toString() {
        return "Person{" +
                "shirt=" + clothes +
                '}';
    }
}
