package HOMEWORKTEST.demo6;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.lang.annotation.Annotation;

public class Demo {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext("HOMEWORKTEST.demo6");
        Person p=context.getBean(Person.class);
        System.out.println(p);
        System.out.println("demo6");
    }


}
